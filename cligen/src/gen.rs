// Everything in this file is intended for usage by generated code.

use std::cmp;
use std::convert::AsRef;
use std::env;
use std::fmt;
use std::fs::{read_dir, File};
use std::io::{BufRead, BufReader};
use std::path::Path;
use std::str::FromStr;

use super::command::ParseError;
use super::CompletionItem;
use super::PARSE_FAILURE_EXIT;

use unicode_segmentation::UnicodeSegmentation;

const MAX_HELP_LINE_LEN_ENV_VAR: &'static str = "CLIGENRS_MAX_HELP_LINE_LEN";
const HELP_LINE_REASONABLE_FIT_ENV_VAR: &'static str = "CLIGENRS_HELP_LINE_REASONABLE_FIT";
const HELP_LINE_SPACING_ENV_VAR: &'static str = "CLIGENRS_HELP_LINE_SPACING";
const MAX_HELP_LINE_LEN: usize = 100;
const HELP_LINE_REASONABLE_FIT: usize = MAX_HELP_LINE_LEN - 25;
const HELP_LINE_SPACING: usize = 4;

pub fn parse<F>(arg: &str, val: &str, ty: &str, name: Option<&str>) -> Result<F, ParseError>
where
    F: FromStr,
{
    let parsed: F = str::parse(val).map_err(|_e| {
        ParseError::ConversionFailed(val.into(), ty.into(), name.unwrap_or(arg).into())
    })?;
    Ok(parsed)
}

pub fn parse_opt<F>(
    arg: &str,
    opts: &Option<&str>,
    ty: &str,
    name: Option<&str>,
) -> Result<F, ParseError>
where
    F: FromStr,
{
    if let Some(s) = opts {
        parse(arg, &s, ty, name)
    } else {
        Err(ParseError::MissingArgument(arg.into()))
    }
}

pub fn args_from_file<P>(p: &P) -> Result<Vec<String>, ParseError>
where
    P: AsRef<Path>,
{
    let file = File::open(p).map_err(|e| {
        ParseError::GenericError(format!(
            "failed to open file {}: {}",
            p.as_ref().display(),
            &e
        ))
    })?;
    let reader = BufReader::new(file);
    let file_args = reader
        .lines()
        .map(|r| {
            r.map_err(|e| {
                eprintln!(
                    "couldn't read arguments from file {}: {}",
                    p.as_ref().display(),
                    &e
                );
                ::std::process::exit(PARSE_FAILURE_EXIT);
            })
            .unwrap()
        })
        .filter(|ref l| l.len() > 0 && !l.trim_start().starts_with("#"))
        .collect();
    Ok(file_args)
}

pub fn autocomplete_args_subcommands(
    inc: &str,
    potential_args: &[CompletionItem],
    subcommands: &[CompletionItem],
) -> Vec<CompletionItem> {
    let inc_empty = inc.len() == 0;

    let mut vec: Vec<CompletionItem> = potential_args
        .iter()
        .filter(|a| inc_empty || a.value.starts_with(inc))
        .map(|c| c.clone())
        .collect();

    vec.extend(
        subcommands
            .iter()
            .filter(|s| inc_empty || s.value.starts_with(inc))
            .map(|c| c.clone()),
    );

    return vec;
}

fn dir_entries_autocomplete(dir: &Path, prefix: &str, into: &mut Vec<CompletionItem>) {
    let entries = match read_dir(dir) {
        Ok(e) => e,
        Err(_) => return,
    };
    let pb = dir.to_path_buf();
    for ent in entries {
        if let Ok(e) = ent {
            let oss = e.file_name();
            if let Some(s) = oss.to_str() {
                if prefix.len() == 0 || s.starts_with(prefix) {
                    let mut full = pb.clone();
                    full.push(s);
                    if let Some(fulls) = full.to_str() {
                        let item =
                            CompletionItem::relative_path(fulls).unwrap_or_else(|| fulls.into());
                        into.push(item);
                    }
                }
            }
        }
    }
}

pub fn allowed_autocomplete(allowed: &[&str], inc: &str) -> Vec<CompletionItem> {
    if inc.len() == 0 {
        return allowed.iter().map(|s| (*s).into()).collect();
    }
    let mut ret = Vec::new();
    for v in allowed {
        if v.starts_with(inc) {
            ret.push((*v).into());
        }
    }
    ret
}

pub fn path_autocomplete(inc: &str) -> Vec<CompletionItem> {
    let mut results = Vec::new();
    let expanded = match shellexpand::full(inc) {
        Ok(s) => s,
        Err(_) => return results,
    };
    let cwd = std::env::current_dir()
        .map_err(|_e| std::process::exit(1))
        .unwrap();
    // TODO
    let ep = if expanded.len() == 0 {
        cwd.clone()
    } else {
        let s: &str = &expanded;
        Path::new(s).to_path_buf()
    };
    // If it is a dir just explore the dir
    if ep.is_dir() {
        dir_entries_autocomplete(&ep, "", &mut results);
    } else {
        // Otherwise explore the parent dir
        let inc = match ep.file_name() {
            Some(oss) => match oss.to_str() {
                Some(s) => s,
                None => return results,
            },
            None => return results,
        };
        let dirname = match ep.parent() {
            Some(p) => {
                if p.as_os_str().len() == 0 {
                    cwd.clone()
                } else {
                    p.to_path_buf()
                }
            }
            None => return results,
        };

        dir_entries_autocomplete(&dirname, &inc, &mut results);
    }
    results
}

fn get_usize_env_default(key: &str, d: usize, min: usize) -> usize {
    env::var(key).map_or(d, |s| {
        cmp::max(str::parse(&s).map_err(|_e| d).unwrap(), min)
    })
}

pub struct OptionHelp<'a> {
    pub prefix: &'a str,
    pub help: &'a str,
}

pub struct OptionHelps<'a> {
    pub option_helps: Vec<OptionHelp<'a>>,
}

impl<'a> fmt::Display for OptionHelps<'a> {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        let max_line_len = get_usize_env_default(MAX_HELP_LINE_LEN_ENV_VAR, MAX_HELP_LINE_LEN, 80);
        let spacing = get_usize_env_default(HELP_LINE_SPACING_ENV_VAR, HELP_LINE_SPACING, 4);
        let reasonable_fit = get_usize_env_default(
            HELP_LINE_REASONABLE_FIT_ENV_VAR,
            HELP_LINE_REASONABLE_FIT,
            50,
        );
        let mut longest_reasonable_prefix = 0;
        // Have to determine the longest prefix before anything
        for oh in &self.option_helps {
            let l = oh.prefix.len();
            if l > longest_reasonable_prefix && l <= max_line_len - spacing - reasonable_fit {
                longest_reasonable_prefix = l;
            }
        }
        for oh in &self.option_helps {
            write!(f, "{}", oh.prefix)?;
            let plen = oh.prefix.len();
            if oh.help.len() > 0 {
                let remaining_space = max_line_len - plen - spacing;
                if oh.help.len() <= remaining_space {
                    let spaces = longest_reasonable_prefix - plen + spacing;
                    for _ in 0..spaces {
                        write!(f, " ")?;
                    }
                    write!(f, "{}", oh.help)?;
                } else if remaining_space >= reasonable_fit {
                    let spaces = longest_reasonable_prefix - plen + spacing;
                    for _ in 0..spaces {
                        write!(f, " ")?;
                    }
                    let mut new_line_prefix = String::new();
                    for _ in 0..longest_reasonable_prefix + 4 {
                        new_line_prefix.push(' ');
                    }
                    let help_string =
                        break_string_into_lines_length_n(oh.help, &new_line_prefix, max_line_len);
                    write!(f, "{}", help_string)?;
                } else {
                    let new_line_prefix: &str = "    ";
                    let help_string =
                        break_string_into_lines_length_n(oh.help, new_line_prefix, max_line_len);
                    write!(f, "\n{}{}", new_line_prefix, help_string)?;
                }
            }
            write!(f, "\n")?;
        }
        Ok(())
    }
}

pub fn break_string_into_lines_length_n(s: &str, new_line_prefix: &str, max: usize) -> String {
    let real_max = max - new_line_prefix.len();
    if s.len() <= real_max {
        return String::from(s);
    }
    let formatted = s.lines().fold(String::new(), |mut acc, v| {
        let mut size = 0;
        // split_word_bounds is useful here because it should preserve all of
        // the unicode.
        for s in v.split_word_bounds() {
            let slen = s.len();
            let new_size = size + slen;
            if new_size >= real_max {
                acc.push('\n');
                acc.push_str(new_line_prefix);
                size = slen;
                // If it is all whitespace we'll ignore it on the newline; that
                // seems reasonable and makes things a bit cleaner.
                let ignore = s.chars().all(|c| c.is_whitespace());
                if !ignore {
                    acc.push_str(&s);
                }
            } else {
                size = new_size;
                acc.push_str(&s);
            }
        }
        acc.push('\n');
        acc.push_str(new_line_prefix);
        acc
    });
    formatted.trim_end().into()
}

#[cfg(test)]
mod tests {

    use super::*;
    use std::env;
    use std::fs::{remove_dir_all, DirBuilder, File};
    use std::io::Write;
    use std::panic;
    use std::path::PathBuf;

    #[test]
    fn test_display_simple() {
        let ohs = OptionHelps {
            option_helps: vec![
                OptionHelp {
                    prefix: "-h/--help",
                    help: "help",
                },
                OptionHelp {
                    prefix: "-v/--verbose",
                    help: "verbose",
                },
            ],
        };
        let s = ohs.to_string();
        assert_eq!(
            s,
            r#"-h/--help       help
-v/--verbose    verbose
"#
        );
    }

    #[test]
    fn test_display_resize() {
        let ohs = OptionHelps {
            option_helps: vec![
                OptionHelp {
                    prefix: "-h/--help",
                    help: "help",
                },
                OptionHelp {
                    prefix: "-v/--verbose",
                    help: "This should be multiple lines when displayed so make this very long la la la la la hello",
                },
            ],
        };
        let s = ohs.to_string();
        assert_eq!(
            s,
            r#"-h/--help       help
-v/--verbose    This should be multiple lines when displayed so make this very long la la la la la 
                hello
"#
        );
    }

    #[test]
    fn test_creates_newline_for_long_prefix() {
        let ohs = OptionHelps {
            option_helps: vec![
                OptionHelp {
                    prefix: "-h/--help",
                    help: "Show this help text and exit",
                },
                OptionHelp {
                    prefix: "-v/--verbose",
                    help: "This should be multiple lines when displayed so make this very long la la la la la hello",
                },
                OptionHelp {
                    prefix: "--very-long-argument-why-are-you-making-your-cli-so-complicated",
                    help: "This should be multiple lines when displayed so make this very long la la la la la hello",
                },
            ],
        };
        let s = ohs.to_string();
        assert_eq!(
            s,
            r#"-h/--help       Show this help text and exit
-v/--verbose    This should be multiple lines when displayed so make this very long la la la la la 
                hello
--very-long-argument-why-are-you-making-your-cli-so-complicated
    This should be multiple lines when displayed so make this very long la la la la la hello
"#
        );
    }

    #[test]
    fn test_args_from_file() {
        let sub = "test_args_from_file";
        run_test_with_tempdir(sub, || {
            let mut td = get_tmp_dir(sub);
            td.push("args");
            {
                let mut f = File::create(&td).unwrap();
                f.write_all(b"\n\n# Comment\n-#\n100\n--flag\nvalue\n  # Comment 2\n--verbose")
                    .unwrap();
            }
            let args = args_from_file(&td).unwrap();
            assert_eq!(
                args.as_slice(),
                &["-#", "100", "--flag", "value", "--verbose"]
            );
        });
    }

    //#[test]
    //fn test_autocomplete_args_subcommand() {
    //    let args = &["-h", "-@", "--help", "--long", "-s", "--foo", "--food"];
    //    let subcommands = &["foo", "bar", "baz"];

    //    let mut all = Vec::new();
    //    all.extend_from_slice(args);
    //    all.extend_from_slice(subcommands);
    //    let mut result = autocomplete_args_subcommands("", args, subcommands);
    //    assert_eq!(result, all);
    //    result = autocomplete_args_subcommands("-", args, subcommands);
    //    assert_eq!(result.as_slice(), args);
    //    result = autocomplete_args_subcommands("--foo", args, subcommands);
    //    assert_eq!(result.as_slice(), &["--foo", "--food"]);
    //    result = autocomplete_args_subcommands("b", args, subcommands);
    //    assert_eq!(result.as_slice(), &["bar", "baz"]);
    //}

    fn get_tmp_dir(sub: &str) -> PathBuf {
        let mut td = env::temp_dir();
        td.push(sub);
        td
    }

    fn setup_tmp_dir(sub: &str) {
        let files = &["foo", "foo1", "foo2", "fo_o", "f0o", "fOO", "bar", "barz"];
        let mut td = get_tmp_dir(sub);
        let db = DirBuilder::new();
        db.create(&td).unwrap();
        for f in files {
            let mut d = td.clone();
            d.push(f);
            File::create(&d).unwrap();
        }
        td.push("foo3");
        db.create(&td).unwrap();
        for f in files {
            let mut d = td.clone();
            d.push(f);
            File::create(&d).unwrap();
        }
    }

    fn teardown_tmp_dir(sub: &str) {
        let td = get_tmp_dir(sub);
        remove_dir_all(&td).unwrap();
    }

    #[test]
    fn test_env_var_file() {
        let sub = "cligen_test_env_var_file";
        run_test_with_tempdir(sub, || {
            let key = "___CLIGENRS_TEMP_DIR";
            let td = get_tmp_dir(sub);

            env::set_var(key, td.to_str().unwrap());

            let mut expected = ["foo", "foo1", "foo2", "foo3/"]
                .iter()
                .map(|s| {
                    let mut cpy = td.clone();
                    cpy.push(s);
                    String::from(cpy.to_str().unwrap())
                })
                .collect::<Vec<String>>();
            expected.sort();
            let dir = format!("${}", key);
            let child_path = Path::new(&dir);
            let mut child = child_path.to_path_buf();
            child.push("foo");

            let mut results = path_autocomplete(child.to_str().unwrap());
            results.sort();

            env::remove_var(key);

            assert_eq!(results, expected,);
        });
    }

    #[test]
    fn test_dir_file() {
        let sub = "cligen_test_dir_file";
        run_test_with_tempdir(sub, || {
            let td = get_tmp_dir(sub);
            let mut expected = ["foo", "foo1", "foo2", "foo3/"]
                .iter()
                .map(|s| {
                    let mut cpy = td.clone();
                    cpy.push(s);
                    String::from(cpy.to_str().unwrap())
                })
                .collect::<Vec<String>>();
            expected.sort();
            let mut child = td.clone();
            child.push("foo");
            let mut results = path_autocomplete(child.to_str().unwrap());
            results.sort();
            assert_eq!(results, expected,);
        });
    }

    #[test]
    fn test_dir_partial() {
        let sub = "cligen_test_dir_partial";
        run_test_with_tempdir(sub, || {
            let td = get_tmp_dir(sub);
            let mut expected = ["foo", "foo1", "foo2", "foo3/", "fo_o"]
                .iter()
                .map(|s| {
                    let mut cpy = td.clone();
                    cpy.push(s);
                    String::from(cpy.to_str().unwrap())
                })
                .collect::<Vec<String>>();
            expected.sort();
            let mut child = td.clone();
            child.push("fo");
            let mut results = path_autocomplete(child.to_str().unwrap());
            results.sort();
            assert_eq!(results, expected,);
        });
    }

    #[test]
    fn test_explores_dir() {
        let sub = "cligen_test_explores_dir";
        run_test_with_tempdir(sub, || {
            let td = get_tmp_dir(sub);
            let mut expected = [
                "", "foo", "foo1", "foo2", "foo3/", "fo_o", "f0o", "fOO", "bar", "barz",
            ]
            .iter()
            .map(|s| {
                let mut cpy = td.clone();
                cpy.push(s);
                String::from(cpy.to_str().unwrap())
            })
            .collect::<Vec<String>>();
            expected.sort();
            let mut results = path_autocomplete(td.to_str().unwrap());
            results.sort();
            assert_eq!(results, expected,);
        });
    }

    fn run_test_with_tempdir<T>(sub: &str, test: T) -> ()
    where
        T: FnOnce() -> () + panic::UnwindSafe,
    {
        setup_tmp_dir(sub);

        let result = panic::catch_unwind(|| test());

        teardown_tmp_dir(sub);

        if let Err(err) = result {
            panic::resume_unwind(err);
        }
    }
}
