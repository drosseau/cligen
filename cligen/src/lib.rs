mod command;
pub use command::{Command, CommandError, CommandResult, ParseError, ParseLeftovers, ParseResult};

pub mod gen;

pub mod autocomplete;
pub use autocomplete::{print_autocompletions, split_args, CompletionItem};

pub static PARSE_FAILURE_EXIT: i32 = 64;
pub static HELP_EXIT: i32 = 126;

#[macro_export]
macro_rules! cligen_main {
    ($cmd:ident) => {{
        $crate::cligen_autocomplete!($cmd);
        $crate::cligen_main!($cmd, no_autocomplete);
    }};
    ($cmd:ident, no_autocomplete) => {{
        let mut c: $cmd = ::std::default::Default::default();
        let args = ::std::env::args();
        let mut it = args.into_iter();
        // TODO This macro skips the base name. I think that is fine actully
        // because if people want to do anything more interesting, they can
        // jsut write their own.
        it.next();
        let leftovers = c
            .parse(&mut it)
            .map_err(|e| {
                eprintln!("{}", $cmd::help());
                eprintln!("\n{}", e);
                ::std::process::exit(::cligen::PARSE_FAILURE_EXIT);
            })
            .unwrap();
        let exit_code: i32 = c.exec(&leftovers).map_or_else(
            |e| {
                eprintln!("{}", e.reason);
                e.exit_code
            },
            |code| code,
        );
        ::std::process::exit(exit_code);
    }};
}
