use std::fmt;
use std::io;
use thiserror::Error;

#[derive(Debug)]
pub struct CommandError {
    pub reason: String,
    pub exit_code: i32,
}

impl CommandError {
    pub fn new(reason: &str, exit: i32) -> Self {
        Self {
            reason: reason.into(),
            exit_code: exit,
        }
    }

    pub fn new_err(reason: &str, exit: i32) -> CommandResult {
        Err(Self::new(reason, exit))
    }
}

impl From<io::Error> for CommandError {
    fn from(err: io::Error) -> Self {
        Self {
            reason: err.to_string(),
            exit_code: 1,
        }
    }
}

impl fmt::Display for CommandError {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "{} (exit code: {})", self.reason, self.exit_code)
    }
}

impl std::error::Error for CommandError {}

/// The command result is either an exit code or an error
pub type CommandResult = Result<i32, CommandError>;

#[derive(Debug, Error)]
pub enum ParseError {
    #[error("unknown argument {0}")]
    UnknownArgument(String),
    #[error("missing required {0}")]
    MissingRequired(String),
    #[error("missing argument for option {0}")]
    MissingArgument(String),
    #[error("missing positional argument at {0} ({1})")]
    MissingPositional(i32, String),
    #[error("failed to convert `{0}` to {1} for option {2}")]
    ConversionFailed(String, String, String),
    #[error("argument `{0}` is malformed")]
    MalformedArgument(String),
    #[error("unexpected positional argument `{0}`")]
    UnexpectedPositionalArg(String),
    #[error("unknown subcommand `{0}`")]
    UnknownSubcommand(String),
    #[error("error in argument {0}: {1}")]
    ActionError(String, String),
    #[error("value `{0}` not allowed for {1}\nallowed = {2:?}")]
    ValueNotAllowed(String, String, Vec<String>),
    #[error("invalid value for {0}: {1}")]
    InvalidValue(String, String),
    #[error("{0}")]
    GenericError(String),
}

#[derive(Debug)]
pub struct ParseLeftovers {
    pub subcommand: Option<String>,
    pub remaining: Option<Vec<String>>,
}

impl ParseLeftovers {
    pub fn empty() -> Self {
        Self {
            subcommand: None,
            remaining: None,
        }
    }

    pub fn with_remaining<T>(it: &mut T) -> Self
    where
        T: Iterator<Item = String>,
    {
        let rem: Vec<String> = it.collect();
        Self {
            subcommand: None,
            remaining: if rem.len() > 0 { Some(rem) } else { None },
        }
    }

    pub fn with_subcommand<T>(sub: &str, it: &mut T) -> Self
    where
        T: Iterator<Item = String>,
    {
        let rem: Vec<String> = it.collect();
        Self {
            subcommand: Some(sub.into()),
            remaining: if rem.len() > 0 { Some(rem) } else { None },
        }
    }
}

pub type ParseResult = Result<ParseLeftovers, ParseError>;

/// Command is the primary trait in this crate. Something that is a Command
/// can be added to the main command and run.
pub trait Command {
    fn parse<T>(&mut self, it: &mut T) -> ParseResult
    where
        T: Iterator<Item = String>;
    fn exec(&self, remaining: &ParseLeftovers) -> CommandResult;
}
