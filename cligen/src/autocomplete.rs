use shlex;
use std::path::Path;

#[macro_export]
macro_rules! cligen_autocomplete {
    ($cmd:ident) => {{
        let ev = ::std::env::var("_CLIGENRS_AUTOCOMPLETE");
        let do_autocomplete = match ::std::env::var("_CLIGENRS_AUTOCOMPLETE") {
            Ok(_) => true,
            Err(e) => match e {
                ::std::env::VarError::NotPresent => false,
                _ => ::std::process::exit(1),
            },
        };
        if do_autocomplete {
            let cwords = if let Ok(cw) = ::std::env::var("COMP_WORDS") {
                cw
            } else {
                ::std::process::exit(1);
            };
            let cword = if let Ok(cw) = ::std::env::var("COMP_CWORD") {
                cw
            } else {
                ::std::process::exit(1);
            };
            let args = $crate::split_args(&cwords);
            let len = args.len();
            if len == 0 {
                let completed = $cmd::arg_autocomplete(&[], "");
                $crate::print_autocompletions(&completed);
                ::std::process::exit(0);
            }
            let cword = if let Ok(cw) = ::std::env::var("COMP_CWORD") {
                cw
            } else {
                ::std::process::exit(1);
            };
            // TODO
            let incomplete_index: usize = if let Ok(idx) = str::parse(&cword) {
                idx
            } else {
                ::std::process::exit(1);
            };
            let mapped = args.iter().map(|ref s| s.as_str()).collect::<Vec<&str>>();
            let completed = if incomplete_index < len {
                let (left, right) = mapped.split_at(incomplete_index);
                let inc = right[0];
                let right = &right[1..];
                let refs = [left, right].concat();
                $cmd::arg_autocomplete(&refs, inc)
            } else {
                let refs = mapped.as_slice();
                $cmd::arg_autocomplete(refs, "")
            };
            $crate::print_autocompletions(&completed);
            ::std::process::exit(0);
        }
    }};
}

enum Shell {
    BASH,
    ZSH,
    UNSUPPORTED,
}

impl From<&String> for Shell {
    fn from(s: &String) -> Shell {
        Shell::from(s.as_str())
    }
}

impl From<&str> for Shell {
    fn from(s: &str) -> Shell {
        match s {
            "bash" => Shell::BASH,
            "zsh" => Shell::ZSH,
            _ => Shell::UNSUPPORTED,
        }
    }
}

#[derive(Debug, Clone)]
pub struct CompletionItem {
    pub value: String,
    pub help: Option<String>,
}

impl CompletionItem {
    pub fn relative_path<P: AsRef<Path> + ?Sized>(p: &P) -> Option<Self> {
        let ap: &Path = p.as_ref();
        let cwd = std::env::current_dir().ok()?;
        let value = if ap.is_absolute() {
            ap.strip_prefix(cwd).ok().unwrap_or(ap)
        } else {
            ap
        };
        Some(Self {
            value: value.to_str()?.into(),
            help: None,
        })
    }
}

impl From<String> for CompletionItem {
    fn from(s: String) -> Self {
        Self {
            value: s,
            help: None,
        }
    }
}

impl From<&str> for CompletionItem {
    fn from(s: &str) -> Self {
        Self {
            value: s.into(),
            help: None,
        }
    }
}

impl CompletionItem {
    pub fn new(value: &str, help: Option<&str>) -> Self {
        Self {
            value: value.into(),
            help: help.map(|s| s.into()),
        }
    }
}

fn print_zsh_autocompletions(comps: &Vec<CompletionItem>) {
    for opt in comps {
        if let Some(ref help) = opt.help {
            println!("{}\n{}", opt.value, help);
        } else {
            println!("{}\n_", opt.value);
        }
    }
}

fn print_bash_autocompletions(comps: &Vec<CompletionItem>) {
    for opt in comps {
        println!("{}", opt.value);
    }
}

pub fn print_autocompletions(comps: &Vec<CompletionItem>) {
    let shell =
        ::std::env::var("_CLIGENRS_AUTOCOMPLETE").map_or_else(|_| Shell::BASH, |s| Shell::from(&s));
    match shell {
        Shell::BASH => print_bash_autocompletions(comps),
        Shell::ZSH => print_zsh_autocompletions(comps),
        Shell::UNSUPPORTED => print_bash_autocompletions(comps),
    };
}

pub fn split_args(cwords: &str) -> Vec<String> {
    let split = shlex::split(&cwords)
        .ok_or_else(|| std::process::exit(1))
        .unwrap();
    split.into_iter().skip(1).collect()
}
