# cligen

`cligen` generates CLI applications from structs.

It supports the following features:

- Short (`-h`) and long (`--help`) flags and options
- Subcommands
- Custom autocompletion
- Custom validation
- Reading all flags from a file (with `-@ FILE`)
- Transforming args to `&'static str` via intentionally leaked pointers

## Flag Files

All CLIs generated with this tool support `-@ FILE` to read flags from files. These files should be formatted as follows:

```
# Comments are allowed, but the # must be at the start of the line
-v
-t
T ARG GOES HERE
--name
NAME ARG GOES HERE
```

## Usage

Simply create a struct with `#[derive(Command)]` and then use `cligen_main!` inside of your main function. This will enable autocomplete and code generation.

See the examples directory for some example applications.
