pub fn help() -> ::std::string::String {
    let include_name = ::std::env::args().any(|a| a == {{ name }});
    let mut usage: ::std::string::String = ::std::env::args().filter(
        |s| !s.starts_with("-")
    ).take_while(
        |s| s != {{ name }}
    ).fold(
        ::std::string::String::from("Usage:"),
        |mut acc, s| {
            {# We don't want the fully qualified path #}
            let p = ::std::path::Path::new(&s);
            let to_add = p.file_name().map_or_else(
                || { s.clone() },
                |n| { String::from(n.to_str().unwrap()) }
            );
            acc.push(' ');
            acc.push_str(&to_add);
            acc
        }
    );
    if include_name {
        usage.push(' ');
        usage.push_str({{ name }});
    }
    {% if has_opts %}
    usage.push_str(" [OPTIONS]");
    {% endif %}
    {% for p in pos %}
    {# > 2 because it is quoted so "" is included #}
    usage.push(' ');
    {% if !p.required %}
    usage.push('[');
    {% endif %}
    {% if p.meta.len() > 2 %}
    usage.push_str({{ p.meta }});
    {% else %}
    usage.push_str("ARG{{ p.pos }}");
    {% endif %}
    {% if !p.required %}
    usage.push(']');
    {% endif %}
    {% endfor %}
    usage.push('\n');
    {% if long_desc.len() > 0 %}
    usage.push_str("\n    ");
    let long_help = ::cligen::gen::break_string_into_lines_length_n(
        {{ long_desc }}, "    ", 80
    );
    usage.push_str(&long_help);
    usage.push('\n');
    {% endif %}
    {% if has_opts %} 
    usage.push_str("\nOptions:\n\n");
    let helps = ::cligen::gen::OptionHelps {
        option_helps: vec![
    {% for opt in options %}
        {% let help = opt.help.as_ref() -%}
        {% let short = opt.short.as_ref() -%}
        {% let long = opt.long.as_ref() -%}
        {% let has_short = short.is_some() -%}
        {% let has_long = long.is_some() -%}
        ::cligen::gen::OptionHelp{
        {% if has_short && has_long %}
            prefix: concat!(
                "-",
                stringify!({{ short.unwrap() }}),
                "/--",
                {{ long.unwrap() }}
            ),
        {% else if has_short %}
            prefix: concat!(
                "-",
                stringify!({{ short.unwrap() }})
            ),

        {% else if has_long %}
            prefix: concat!(
                "--",
                {{ long.unwrap() }}
            ),
        {% endif %}
        {% if help.is_some() %}
            {% if opt.required %}
            help: concat!({{ help.unwrap() }}, " [Required]"),
            {% else %}
            help: {{ help.unwrap() }},
            {% endif %}
        {% else %}
            help: "",
        {% endif %}
        },
    {% endfor %}
        ]
    };
    let disp = helps.to_string();
    usage.push_str(&disp);
    {% endif %}
    {% if has_subcommands %}
    usage.push_str("\nSubcommands:\n\n");
    let subcommand_helps = ::cligen::gen::OptionHelps {
        option_helps: vec![
        {% for sub in subcommands %}
        ::cligen::gen::OptionHelp{
            prefix: {{ sub.name }},
            help: {{ sub.target }}::short_desc(),
        },
        {% endfor %}
        ]
    };
    let subcommand_disp = subcommand_helps.to_string();
    usage.push_str(&subcommand_disp);
    {% endif %}
    usage
}
