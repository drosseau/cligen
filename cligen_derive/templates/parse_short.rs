if arg.starts_with("-") {
    let c = arg.chars().nth(1).ok_or_else(
        || ::cligen::ParseError::MalformedArgument(arg.clone())
    )?;
    match c {
    '@' => {
        let val = it.next().ok_or_else(|| {
            ::cligen::ParseError::MissingArgument("-@".into())
        })?;
        let mut vargs = ::cligen::gen::args_from_file(&val)?;
        let mut file_args_it = vargs.drain(0..);
        return self.parse(&mut file_args_it);
    }
    'h' => {
        eprintln!("{}", Self::help());
        ::std::process::exit(::cligen::HELP_EXIT);
    }

    {% for opt in options %}
        {% let short = opt.short.as_ref() -%}
        {% if short.is_some() -%}

    '{{ short.unwrap() }}' => {
            {% if opt.is_flag %}
        self.{{ opt.field }} = !{{ opt.default.as_ref().unwrap() }};
            {% else %}
        let val = it.next().ok_or_else(|| {
            ::cligen::ParseError::MissingArgument(arg.into())
        })?;

                {% let allowed_ref = opt.allowed.as_ref() %}
                {% if allowed_ref.is_some() %}
        let {{ opt.field }}_allowed: &[&str] = &[
            {{ allowed_ref.unwrap().to_vec() | quote_all_if_unquoted | join(", ") }}
        ];
        if !{{ opt.field }}_allowed.iter().any(|e| *e == &val) {
            return Err(
                ::cligen::ParseError::ValueNotAllowed(
                    val.clone(),
                    "-{{ short.unwrap() }}".to_string(),
                    {{ opt.field }}_allowed.iter().map(|s| s.to_string()).collect()
                )
            );
        }
                {% endif %}

                {% if opt.action.is_some() %}
            {{ opt.action.as_ref().unwrap() }}(self, &val)?;
                {% else %}
            let parsed_{{ opt.field }} = {% if opt.is_option %}Some({% endif %}
                {% if opt.is_static_str() %}
                ::std::boxed::Box::leak(val.into_boxed_str()) as &'static str
                {% else %}
                ::cligen::gen::parse(
                    &arg, &val, stringify!({{ opt.ty }}), None
                )?
                {% endif %}
            {% if opt.is_option %}){% endif %};
                {% endif %}

            self.{{ opt.field }}{% if opt.multi %}
                .push(parsed_{{ opt.field }});
            {% else %}
                = parsed_{{ opt.field }};
            {% endif %}
            {#

                {% else if opt.multi %}
            self.{{ opt.field }}.push(::cligen::gen::parse(
                &arg, &val, stringify!({{ opt.ty }}), None
            )?);
                {% else if opt.is_option %}
            self.{{ opt.field }} = Some(::cligen::gen::parse(
                &arg, &val, stringify!({{ opt.ty }}), None
            )?);
                {% else %}
            self.{{ opt.field }} = ::cligen::gen::parse(
                &arg, &val, stringify!({{ opt.ty }}), None
            )?;
                {% endif %}

            #}

                {% let validate_ref = opt.validate.as_ref() %}
                {% if validate_ref.is_some() %}
            if !{{ validate_ref.unwrap() }}(&self.{{ opt.field }}) {
                return Err(
                    ::cligen::ParseError::InvalidValue(
                        "-{{ short.unwrap() }}".into(), val.clone() 
                    )
                );
            }
                {% endif %}
            {% endif %}
    },
        {% endif %}

    {% endfor -%}

    _ => {
        return Err(::cligen::ParseError::UnknownArgument(arg.into()));
    },
    }
