pub fn arg_autocomplete(args: &[&str], inc: &str) -> ::std::vec::Vec<::cligen::CompletionItem> {
    let all_args = &[
        ::cligen::CompletionItem::new("-h", Some("display help and exit")),
        {% if uses_long %}
        ::cligen::CompletionItem::new("--help", Some("display help and exit")),
        {% endif %}
        ::cligen::CompletionItem::new("-@", Some("read args from a file")),
    {% for opt in options %}
        {% let long = opt.long.as_ref() -%}
        {% let short = opt.short.as_ref() -%}
        {# TODO complexity #}
        {% let help = opt.help.as_ref().unwrap_or("".into()).clone() -%}
        {% if long.is_some() %}
        ::cligen::CompletionItem::new(
            concat!("--", {{ long.unwrap() }}),
            Some({{ help }})
        ),
        {% endif %}
        {% if short.is_some() %}
        ::cligen::CompletionItem::new(
            concat!("-", stringify!({{ short.unwrap() }})),
            Some({{ help }})
        ),
        {% endif %}
    {% endfor %}
    ];

    let all_subcommands = &[
    {% for s in subcommands %}
        ::cligen::CompletionItem::new({{ s.name }}, Some({{ s.target }}::short_desc())),
    {% endfor %}
    ];

    let nargs = args.len();

    {#
        nargs == 0 is the simplest case, we can just return all_args and 
        all_subcommands provided they're prefixed with inc
    #}

    if nargs == 0 {
        {#
            If they have positional autocompleters we want to return both
            the autocomplete_args_subcommands and the invoked autocompleter
            for that position.
        #}
        {% if has_pos %}
            {% let p = pos[0] %}
            {% if p.autocomplete.is_some() %}
                let mut potential = ::cligen::gen::autocomplete_args_subcommands(
                    inc, all_args, all_subcommands
                );
                potential.extend({{ p.autocomplete.as_ref().unwrap() }}(args, inc));
                return potential;
            {% else %}
                return ::cligen::gen::autocomplete_args_subcommands(
                    inc, all_args, all_subcommands
                );
            {% endif %}
        {% else %}
        return ::cligen::gen::autocomplete_args_subcommands(
            inc, all_args, all_subcommands
        );
        {% endif %}
    }

    {% if has_subcommands %}
    for (i, a) in args.iter().enumerate() {
        match *a {
        {% for s in subcommands %}
        {{ s.name }} => {
            let (_, new_args) = args.split_at(i);
            return {{ s.target }}::arg_autocomplete(new_args, inc);
        }
        {% endfor %}
        _ => {}
        }
    }
    {% endif %}

    let last = args[nargs - 1];
    if last.starts_with("-") {
        return match last {
        {% for opt in options %}
            {% let opt_ac = opt.autocomplete.as_ref() %}
            {% let opt_allowed = opt.allowed.as_ref() %}
            {% if opt_ac.is_some() %}
                {% let short_opt = opt.short.as_ref() %}
                {% let long_opt = opt.long.as_ref() %}
                {% if long_opt.is_some() %}
        concat!("--", {{ long_opt.unwrap() }}) => {
            {{ opt_ac.unwrap() }}(args, inc)
        },
                {% endif %}
                {% if short_opt.is_some() %}
        concat!("-", stringify!({{ short_opt.unwrap() }})) => {
            {{ opt_ac.unwrap() }}(args, inc)
        },
                {% endif %}
            {% else if opt_allowed.is_some() %}
                {% let short_opt = opt.short.as_ref() %}
                {% let long_opt = opt.long.as_ref() %}
                {% if long_opt.is_some() %}
        concat!("--", {{ long_opt.unwrap() }}) => {
            let allowed = {{ opt_allowed.unwrap().to_vec() | collect_to_str_slice }};
            ::cligen::gen::allowed_autocomplete(allowed, inc)
        },
                {% endif %}
                {% if short_opt.is_some() %}
        concat!("-", stringify!({{ short_opt.unwrap() }})) => {
            let allowed = {{ opt_allowed.unwrap().to_vec() | collect_to_str_slice }};
            ::cligen::gen::allowed_autocomplete(allowed, inc)
        },
                {% endif %}
            {% endif %}
        {% endfor %}
        {% for opt in options %}
            {% let opt_ac = opt.autocomplete.as_ref() %}
            {% if opt.is_flag && opt_ac.is_none() %}
            {% endif %}
        {% endfor %}
            {% if uses_long %}
            "--help" |
            {% endif %}
            "-h" => {
                ::cligen::gen::autocomplete_args_subcommands(
                    inc, all_args, all_subcommands
                )
            }
            "-@" => {
                ::cligen::gen::path_autocomplete(inc)
            }
            _ => {
                ::cligen::gen::autocomplete_args_subcommands(
                    inc, all_args, all_subcommands
                )
            },
        }
    }
    {#
        Last item wasn't an -[-]arg. It could either be a positional value,
        a subcommand, or a new -[-]arg.
    #}
    {% if has_pos %}
    {# TODO #}
    ::cligen::gen::autocomplete_args_subcommands(
        inc, all_args, all_subcommands
    )
    {% else %}
    ::cligen::gen::autocomplete_args_subcommands(
        inc, all_args, all_subcommands
    )
    {% endif %}
}
