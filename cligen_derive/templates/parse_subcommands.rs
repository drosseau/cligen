} else {
match arg.as_str() {

{% for s in subcommands %}

{{ s.name }} => {
    return Ok(
        ::cligen::ParseLeftovers::with_subcommand(
            {{ s.name }}, it
        )
    );
}

{% endfor %}

_ => {
    return Err(::cligen::ParseError::UnknownSubcommand(arg.into()));
}

}
}


