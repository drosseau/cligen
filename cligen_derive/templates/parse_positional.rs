} else {
match pos_count {

{% for p in pos %}

    {{ p.pos }} => {
        {% if p.action.is_some() %}
            {{ p.action.as_ref().unwrap() }}(self, &arg)?;
        {% else %}
        self.{{ p.field }} =
            {% if p.is_static_str() %}
            ::std::boxed::Box::leak(arg.clone().into_boxed_str()) as &'static str;
            {% else %}
            ::cligen::gen::parse(
            "unused", &arg, stringify!({{ p.ty }}), 
            {% if p.meta.len() > 2 %}
            Some({{ p.meta }})
            {% else %}
            Some("ARG{{ p.pos }}")
            {% endif %}
        )?;
            {% endif %}
        {% endif %}
        {% if p.validate.as_ref().is_some() %}
        if !{{ p.validate.as_ref().unwrap() }}(&self.{{ p.field }}) {
            return Err(
                ::cligen::ParseError::InvalidValue(
                    {% if p.meta.len() > 2 %}
                    {{ p.meta }}.into(),
                    {% else %}
                    "ARG{{ p.pos }}".into(),
                    {% endif %}
                    arg.clone()
                )
            );
        }
        {% endif %}
        pos_count += 1;
    },

{% endfor %}

    _ => {
        return Err(::cligen::ParseError::UnexpectedPositionalArg(arg.into()));
    },
}

}
