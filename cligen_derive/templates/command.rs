impl{{ impl_generics }} ::cligen::Command for {{ struct_name }}{{ type_generics }}
{{ where_clause }}
{

    fn parse<ArgIt>(&mut self, it: &mut ArgIt) -> ::cligen::ParseResult
        where ArgIt : Iterator<Item = String>
    {
        {% if has_pos %}

        let mut pos_count = 0;

        {% endif %}

        {% let has_subcommands = subcommands.len() > 0 %}

        while let Some(ref arg) = it.next() {
            //println!("{}", arg);

            {% if uses_long %}
                {% include "parse_long.rs" %}
            {% else %}
            {#
                Even if we're not parsing long arguments, `--` is used to
                indicate parsing is over.
            #}
                if arg == "--" {
                    return Ok(
                        ::cligen::ParseLeftovers::with_remaining(it)
                    );
                }
            {% endif %}
                {% include "parse_short.rs" %}
            {% if has_pos %}
                {% include "parse_positional.rs" %}
            {% else if has_subcommands %}
                {% include "parse_subcommands.rs" %}
            {% else %}
            } else {
                return Err(::cligen::ParseError::MalformedArgument(arg.into()));
            }
            {% endif %}
        }
        Ok(::cligen::ParseLeftovers::empty())
    }

    {% include "exec.rs" %}


}
