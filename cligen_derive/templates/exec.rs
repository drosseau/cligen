{% if has_subcommands %}
fn exec(&self, lo: &::cligen::ParseLeftovers) -> ::cligen::CommandResult {
{% else %}
fn exec(&self, _lo: &::cligen::ParseLeftovers) -> ::cligen::CommandResult {
{% endif %}
    {% if func.len() > 0 %}
    {{ func }}(self)
    {% else if has_subcommands %}
    if let Some(subcommand) = &lo.subcommand {
        match subcommand.as_str() {
        {% for s in subcommands %}
        {{ s.name }} => {
            if let Some(rem) = &lo.remaining {
                let mut sub: {{ s.target }} = ::std::default::Default::default();
                let mut cloned = rem.clone();
                let mut it = cloned.drain(0..);
                let leftovers = sub.parse(&mut it)
                    .map_err(|e| {
                        eprintln!("{}", e);
                        ::std::process::exit(::cligen::PARSE_FAILURE_EXIT);
                    })
                    .unwrap();
                return sub.exec(&leftovers);
            } else {
                let mut sub: {{ s.target }} = ::std::default::Default::default();
                let lo = ::cligen::ParseLeftovers::empty();
                return sub.exec(&lo);
            }
        }
        {% endfor %}
        _ => {
            eprintln!("{}", Self::help());
            eprintln!("\nUnknown subcommand: {}", &subcommand);
            ::std::process::exit(::cligen::HELP_EXIT);
        }
        }
    } else {
        eprintln!("{}", Self::help());
        ::std::process::exit(::cligen::HELP_EXIT);
    }
    {% else %}
    panic!("No func or subcommands give for {{ struct_name }}!")
    {% endif %}
}
