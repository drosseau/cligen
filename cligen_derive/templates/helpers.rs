impl{{ impl_generics }} {{ struct_name }}{{ type_generics }}
{{ where_clause }}
{

    {% let has_subcommands = subcommands.len() > 0 %}

    {% include "autocomplete.rs" %}

    {% include "help.rs" %}
    
    pub fn name() -> &'static str {
        {{ name }}
    }

    pub fn short_desc() -> &'static str {
    {% if short_desc.len() > 0 %}
        {{ short_desc }}
    {% else %}
        ""
    {% endif %}
    }

    pub fn long_desc() -> &'static str {
    {% if long_desc.len() > 0 %}
        {{ long_desc }}
    {% else %}
        ""
    {% endif %}
    }
}
