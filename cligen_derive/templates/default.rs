impl {{ impl_generics }} ::std::default::Default for {{ name }}{{ type_generics }}
{{ where_clause }}
{
    fn default() -> Self {
        Self {
        {% for f in fields %}
            {{ f.name }}: {{ f.default }},
        {% endfor %}
        }
    }
}
