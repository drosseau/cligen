use proc_macro2::Ident;
use syn::parse::{self, Parse, ParseStream};
use syn::punctuated::Punctuated;
use syn::token::Bracket;
use syn::Field;
use syn::{bracketed, LitBool, LitChar, LitStr, Token};

use super::quote_string;

#[derive(Default)]
pub struct Command {
    pub name: String,
    pub func: String,
    pub short_desc: String,
    pub long_desc: String,
    pub subcommands: Vec<Subcommand>,
}

impl Parse for Command {
    fn parse(buf: ParseStream) -> parse::Result<Self> {
        let mut cmd: Self = Default::default();
        loop {
            let ident: syn::Ident = buf.parse()?;
            let _eq: Token![=] = buf.parse()?;
            let name = ident.to_string();
            match name.as_str() {
                "func" => {
                    let s: syn::Ident = buf.parse()?;
                    cmd.func = s.to_string();
                }
                "name" => {
                    let ls: LitStr = buf.parse()?;
                    let s = ls.value();
                    cmd.name = quote_string(&s);
                }
                "short_desc" => {
                    let ls: LitStr = buf.parse()?;
                    let s = ls.value();
                    cmd.short_desc = quote_string(&s);
                }
                "long_desc" => {
                    let ls: LitStr = buf.parse()?;
                    let s = ls.value();
                    cmd.long_desc = quote_string(&s);
                }
                "subcommands" => {
                    /*
                     * subcommands = [
                     *  "...",
                     *  "...",
                     * ]
                     */
                    let toks;
                    bracketed!(toks in buf);
                    let subs: Punctuated<Subcommand, Token![,]> =
                        Punctuated::parse_terminated(&toks)?;
                    cmd.subcommands = subs.into_iter().collect();
                }
                _ => {
                    return Err(parse::Error::new(
                        ident.span(),
                        format!("unexpected attribute for command: {}", &name),
                    ))
                }
            }
            if buf.is_empty() {
                break;
            }
            if !buf.peek(Token![,]) {
                return Err(buf.error("expected ,"));
            } else {
                let _comma: Token![,] = buf.parse()?;
            }
        }
        Ok(cmd)
    }
}

// Subcommands have two forms:
// 1) Ident
// 2) "string" => Ident
#[derive(Debug, Default, Clone)]
pub struct Subcommand {
    pub name: String,
    pub target: String,
}

impl Parse for Subcommand {
    fn parse(buf: ParseStream) -> parse::Result<Self> {
        let mut sub: Self = Default::default();
        if buf.peek(LitStr) {
            let ls: LitStr = buf.parse()?;
            let s = ls.value();
            sub.name = quote_string(&s);
            buf.parse::<Token![=>]>()?;
            let id: Ident = buf.parse()?;
            sub.target = id.to_string();
        } else if buf.peek(syn::Ident) {
            let id: Ident = buf.parse()?;
            sub.target = id.to_string();
            let lc = sub.target.to_lowercase();
            sub.name = quote_string(&lc);
        } else {
            return Err(buf.error("malformed subcommand"));
        }
        Ok(sub)
    }
}
