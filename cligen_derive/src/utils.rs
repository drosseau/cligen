use quote::quote;
use syn::Lit;

pub fn lit_to_string(def: &Lit) -> String {
    match def {
        Lit::Str(s) => {
            let st = s.value();
            format!("{}.into()", quote_string(&st))
        }
        _ => quote!(#def).to_string(),
    }
}

pub fn quote_string(s: &str) -> String {
    let mut new = String::from("\"");
    for c in s.escape_default() {
        new.push(c);
    }
    new.push('"');
    new
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_quote_string() {
        assert_eq!(quote_string("simple"), "\"simple\"");
        assert_eq!(quote_string("with_quote\""), "\"with_quote\\\"\"");
        assert_eq!(quote_string("newline\n"), "\"newline\\n\"");
        assert_eq!(quote_string("tab\t"), "\"tab\\t\"");
    }
}
