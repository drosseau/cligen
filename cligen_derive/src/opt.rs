use proc_macro2::{Ident, TokenStream};
use quote::{quote, ToTokens};
use syn::parse::{self, Parse, ParseStream};
use syn::punctuated::Punctuated;
use syn::token::Bracket;
use syn::Field;
use syn::{bracketed, Lit, LitBool, LitChar, LitStr, Token};

use super::{lit_to_string, quote_string};

#[derive(Default, Clone)]
pub struct Opt {
    pub short: Option<char>,
    pub long: Option<String>,
    pub default: Option<String>,
    pub help: Option<String>,
    pub action: Option<String>,
    pub validate: Option<String>,
    pub allowed: Option<Vec<String>>,
    pub autocomplete: Option<String>,
    pub is_flag: bool,
    pub is_option: bool,
    pub multi: bool,
    pub field: String,
    pub ty: String,
    pub required: bool,
}

impl Opt {
    pub fn update_field(&mut self, field: &Field) -> parse::Result<()> {
        self.field = field.ident.as_ref().unwrap().to_string();
        let ty = &field.ty;
        let s = match ty {
            syn::Type::Path(p) => {
                let s = &p.path;
                Some(quote! {#s}.to_string())
            }
            _ => None,
        }
        .unwrap();
        self.is_flag = s == "bool";
        if self.is_flag && self.default.is_none() {
            self.default = Some("false".into());
        }
        self.is_option = s.starts_with("Option");
        self.ty = s;
        Ok(())
    }

    pub fn is_static_str(&self) -> bool {
        self.ty.contains("'static str")
    }
}

impl Parse for Opt {
    fn parse(buf: ParseStream) -> parse::Result<Self> {
        let mut opt: Self = Default::default();
        loop {
            let ident: Ident = buf.parse()?;
            let _eq: Token![=] = buf.parse()?;
            let name = ident.to_string();
            match name.as_str() {
                "short" => {
                    let c: LitChar = buf.parse()?;
                    let ch = c.value();
                    // Blacklist some chars that will mess with things
                    match ch {
                        '@' | ';' | '-' | '\\' | 'h' | '"' | '\'' => {
                            let es = format!("-{} is not a valid short flag", ch);
                            return Err(parse::Error::new(c.span(), &es));
                        }
                        _ => {
                            opt.short = Some(ch);
                        }
                    }
                }
                "long" => {
                    let ls: LitStr = buf.parse()?;
                    // Don't allow --help
                    let s = ls.value();
                    if s == "help" {
                        return Err(parse::Error::new(ls.span(), "--help is reserved"));
                    }
                    opt.long = Some(quote_string(&s));
                }
                "allowed" => {
                    let toks;
                    bracketed!(toks in buf);
                    let allowed: Punctuated<Lit, Token![,]> = Punctuated::parse_terminated(&toks)?;
                    opt.allowed = Some(
                        allowed
                            .into_iter()
                            .map(|l| quote!(#l).to_string())
                            .collect(),
                    );
                }
                "multi" => {
                    let b: LitBool = buf.parse()?;
                    opt.multi = b.value;
                }
                "help" => {
                    let ls: LitStr = buf.parse()?;
                    let s = ls.value();
                    opt.help = Some(quote_string(&s));
                }
                "required" => {
                    let b: LitBool = buf.parse()?;
                    opt.required = b.value;
                }
                "validate" => {
                    let s: syn::Ident = buf.parse()?;
                    opt.validate = Some(s.to_string());
                }
                "action" => {
                    let s: syn::Ident = buf.parse()?;
                    opt.action = Some(s.to_string());
                }
                "autocomplete" => {
                    let s: syn::Ident = buf.parse()?;
                    opt.autocomplete = Some(s.to_string());
                }
                "default" => {
                    let s: Lit = buf.parse()?;
                    opt.default = Some(lit_to_string(&s));
                }
                _ => {
                    return Err(parse::Error::new(
                        ident.span(),
                        format!("unexpected attribute for option: {}", &name),
                    ))
                }
            }
            if buf.is_empty() {
                break;
            }
            if !buf.peek(Token![,]) {
                return Err(buf.error("expected ,"));
            } else {
                let _comma: Token![,] = buf.parse()?;
            }
        }
        Ok(opt)
    }
}
