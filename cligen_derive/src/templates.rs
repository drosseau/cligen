use askama::Template;

use super::command::Subcommand;
use super::opt::Opt;
use super::positional::Positional;

#[derive(Default, Template)]
#[template(path = "command.rs", escape = "none")]
pub struct CommandTemplate {
    pub name: String,
    pub struct_name: String,
    pub func: String,
    pub impl_generics: String,
    pub where_clause: String,
    pub type_generics: String,
    pub short_desc: String,
    pub long_desc: String,
    pub options: Vec<Opt>,
    pub pos: Vec<Positional>,
    pub subcommands: Vec<Subcommand>,

    pub has_pos: bool,
    pub has_opts: bool,
    pub uses_long: bool,
    pub uses_short: bool,
}

#[derive(Template)]
#[template(path = "helpers.rs", escape = "none")]
pub struct HelpersTemplate<'a> {
    pub name: &'a str,
    pub struct_name: &'a str,
    pub impl_generics: &'a str,
    pub where_clause: &'a str,
    pub type_generics: &'a str,

    pub short_desc: &'a str,
    pub long_desc: &'a str,
    pub options: &'a [Opt],
    pub pos: &'a [Positional],
    pub subcommands: &'a [Subcommand],

    pub has_pos: bool,
    pub has_opts: bool,
    pub uses_long: bool,
    pub uses_short: bool,
}

#[derive(Default, Template)]
#[template(path = "default.rs", escape = "none")]
pub struct DefaultTemplate {
    pub name: String,
    pub impl_generics: String,
    pub where_clause: String,
    pub type_generics: String,
    pub fields: Vec<Field>,
}

#[derive(Debug)]
pub struct Field {
    pub name: String,
    pub ty: String,
    pub default: String,
}

mod filters {

    use askama;

    pub fn collect_to_str_slice(vec: &Vec<String>) -> askama::Result<String> {
        let mut slice = String::from("&[");
        for s in vec.iter() {
            let quoted = quote(s);
            slice.push_str(&format!("{},", quoted));
        }
        slice.push_str("]");
        Ok(slice)
    }

    fn quote(s: &str) -> String {
        let mut new = String::from("\"");
        for c in s.escape_default() {
            new.push(c);
        }
        new.push('"');
        new
    }

    pub fn quote_string(s: &str) -> askama::Result<String> {
        Ok(quote(s))
    }

    pub fn quote_if_unquoted(s: &str) -> askama::Result<String> {
        if s.starts_with("\"") {
            return Ok(s.into());
        }
        quote_string(s)
    }

    pub fn quote_all_if_unquoted(vec: &Vec<String>) -> askama::Result<Vec<String>> {
        return Ok(vec.iter().map(|s| quote_if_unquoted(&s).unwrap()).collect());
    }

    pub fn long_opt_unwrap(s: Option<String>) -> askama::Result<String> {
        let u = s.unwrap();
        long_opt(&u)
    }

    pub fn short_opt_unwrap(s: Option<String>) -> askama::Result<String> {
        let u = s.unwrap();
        short_opt(&u)
    }

    pub fn long_opt(s: &str) -> askama::Result<String> {
        Ok(format!("--{}", s))
    }

    pub fn short_opt(s: &str) -> askama::Result<String> {
        Ok(format!("-{}", s))
    }

    #[cfg(test)]
    mod tests {
        use super::*;

        #[test]
        fn test_quote_string() {
            assert_eq!(quote_string("simple").unwrap(), "\"simple\"");
            assert_eq!(quote_string("with_quote\"").unwrap(), "\"with_quote\\\"\"");
            assert_eq!(quote_string("newline\n").unwrap(), "\"newline\\n\"");
            assert_eq!(quote_string("tab\t").unwrap(), "\"tab\\t\"");
        }
    }
}
