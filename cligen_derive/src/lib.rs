#![allow(dead_code)]
#![allow(unused_imports)]

use askama::Template;

use cligen::gen::break_string_into_lines_length_n;
mod command;
mod utils;
use command::{Command, Subcommand};
use utils::{lit_to_string, quote_string};
mod positional;
use positional::Positional;
mod opt;
use opt::Opt;
mod templates;
use templates::{CommandTemplate, DefaultTemplate, Field, HelpersTemplate};

use std::collections::HashMap;
use std::env;
use std::fmt::{self, Debug};
use std::fs::OpenOptions;
use std::io::Write;

use proc_macro2::{Ident, Literal, TokenStream, TokenTree};
use quote::{quote, ToTokens};
use syn::parse;
use syn::{self, parse_macro_input, Attribute, DeriveInput, ItemImpl, Lit, LitStr};

use unicode_segmentation::UnicodeSegmentation;

#[proc_macro_derive(Command, attributes(command, arg, option, default))]
pub fn cligen_derive(input: proc_macro::TokenStream) -> proc_macro::TokenStream {
    // Parse the input tokens into a syntax tree.
    let input = parse_macro_input!(input as DeriveInput);

    let mut tokens = TokenStream::new();

    // TODO refactor these functions

    derive_command(&input, &mut tokens).map(|cmd| {
        derive_default(&cmd, &input, &mut tokens);
    });
    proc_macro::TokenStream::from(tokens)
}

fn derive_command(input: &DeriveInput, into: &mut TokenStream) -> Option<CommandTemplate> {
    let mut can_impl = true;
    let mut cmd_template: CommandTemplate = Default::default();

    cmd_template.struct_name = input.ident.to_string();

    // TODO error
    let command_attr = input
        .attrs
        .iter()
        .find(is_command_attr)
        .ok_or_else(|| {
            panic!("command not found on {}", &cmd_template.struct_name);
        })
        .unwrap();

    let mut ocmd = command_attr
        .parse_args::<Command>()
        .map_err(|e| {
            can_impl = false;
            e.to_compile_error().to_tokens(into);
            e
        })
        .ok();

    if let Some(ref mut cmd) = ocmd {
        if cmd.name == "" || cmd.name == "\"\"" {
            let lc = cmd_template.struct_name.to_lowercase();
            cmd.name = quote_string(&lc);
        }
        cmd_template.name = cmd.name.clone();
        cmd_template.func = cmd.func.clone();
        cmd_template.subcommands = cmd.subcommands.clone();
    }

    // TODO panic
    let struct_data = match &input.data {
        syn::Data::Struct(s) => s,
        _ => panic!("must be a struct!"),
    };

    // TODO panic. Maybe if they're all unnamed they're just all assumed to be
    // positional.
    let fields = match &struct_data.fields {
        syn::Fields::Named(fs) => fs,
        _ => panic!("must be named fields!"),
    };

    let mut no_more_required_args = false;

    for field in &fields.named {
        let attr = &field.attrs;
        let arg_meta = attr.iter().find(is_field_attr);
        if let Some(am) = arg_meta {
            if am.path.is_ident("arg") {
                if let Some(mut pos) = am
                    .parse_args::<Positional>()
                    .map_err(|e| {
                        can_impl = false;
                        e.to_compile_error().to_tokens(into);
                        e
                    })
                    .ok()
                {
                    pos.update_field(&field).unwrap();
                    if no_more_required_args {
                        if pos.required {
                            let es = format!(
                                "positional argument {} marked as required after nonrequired arg",
                                pos,
                            );
                            let err = parse::Error::new(field.ident.as_ref().unwrap().span(), &es);
                            err.to_compile_error().to_tokens(into);
                        }
                    } else if !pos.required {
                        no_more_required_args = true;
                    }
                    cmd_template.pos.push(pos);
                }
            } else {
                if let Some(mut opt) = am
                    .parse_args::<Opt>()
                    .map_err(|e| {
                        can_impl = false;
                        e.to_compile_error().to_tokens(into);
                        e
                    })
                    .ok()
                {
                    opt.update_field(&field).unwrap();
                    cmd_template.options.push(opt);
                }
            }
        } else {
        }
    }

    if !can_impl {
        let err =
            syn::parse::Error::new(input.ident.span(), "failed to implement Command for struct");
        err.to_compile_error().to_tokens(into);
        return None;
    }

    let (impl_g, ty_g, where_clause) = input.generics.split_for_impl();

    cmd_template.type_generics = quote!(#ty_g).to_string();
    cmd_template.impl_generics = quote!(#impl_g).to_string();
    cmd_template.where_clause = quote!(#where_clause).to_string();

    cmd_template.has_pos = cmd_template.pos.len() > 0;
    cmd_template.has_opts = cmd_template.options.len() > 0;
    cmd_template.uses_long =
        cmd_template.has_opts && cmd_template.options.iter().any(|o| o.long.is_some());
    cmd_template.uses_short =
        cmd_template.has_opts && cmd_template.options.iter().any(|o| o.short.is_some());

    if let Some(cmd) = ocmd {
        if cmd.long_desc.len() > 0 {
            cmd_template.long_desc = cmd.long_desc.clone();
        }
        if cmd.short_desc.len() > 0 {
            cmd_template.short_desc = cmd.short_desc.clone();
        }
    }

    derive_helpers(&cmd_template, input, into);

    let cmd_code = cmd_template
        .render()
        .map_err(|_e| {
            panic!("failed to render Command implementation");
        })
        .unwrap();

    dump_code(&cmd_code, false);

    syn::parse_str::<ItemImpl>(&cmd_code)
        .map_err(|_e| {
            panic!("failed to parse command code as an ItemImpl");
        })
        .unwrap()
        .to_tokens(into);
    Some(cmd_template)
}

fn derive_default(cmd: &CommandTemplate, input: &DeriveInput, into: &mut TokenStream) {
    let mut def_template: DefaultTemplate = Default::default();
    def_template.name = input.ident.to_string();

    def_template
        .fields
        .extend(cmd.options.iter().map(|opt| Field {
            name: opt.field.clone(),
            ty: opt.ty.clone(),
            default: opt.default.as_ref().map_or_else(
                || String::from("::std::default::Default::default()"),
                |s| String::from(s),
            ),
        }));

    def_template.fields.extend(cmd.pos.iter().map(|pos| Field {
        name: pos.field.clone(),
        ty: pos.ty.clone(),
        default: pos.default.as_ref().map_or_else(
            || String::from("::std::default::Default::default()"),
            |s| String::from(s),
        ),
    }));

    // Also need to handle fields that are part of the struct but not parsed

    let (impl_g, ty_g, where_clause) = input.generics.split_for_impl();

    def_template.where_clause = quote!(#where_clause).to_string();
    def_template.impl_generics = quote!(#impl_g).to_string();
    def_template.type_generics = quote!(#ty_g).to_string();

    let def_code = def_template
        .render()
        .map_err(|_e| {
            panic!("failed to render impl Default template");
        })
        .unwrap();

    dump_code(&def_code, true);

    syn::parse_str::<ItemImpl>(&def_code)
        .map_err(|_e| {
            panic!("failed to parse Default code as an ItemImpl");
        })
        .unwrap()
        .to_tokens(into);
}

fn derive_helpers(cmd: &CommandTemplate, input: &DeriveInput, into: &mut TokenStream) {
    let mut can_impl = true;
    let mut helpers_template = HelpersTemplate {
        name: cmd.name.as_str(),
        struct_name: cmd.struct_name.as_str(),
        impl_generics: cmd.impl_generics.as_str(),
        where_clause: cmd.where_clause.as_str(),
        type_generics: cmd.type_generics.as_str(),
        options: cmd.options.as_slice(),
        pos: cmd.pos.as_slice(),
        subcommands: cmd.subcommands.as_slice(),
        has_pos: cmd.has_pos,
        has_opts: cmd.has_opts,
        uses_long: cmd.uses_long,
        uses_short: cmd.uses_short,
        long_desc: "",
        short_desc: "",
    };

    // TODO error
    let command_attr = input
        .attrs
        .iter()
        .find(|&attr| attr.path.is_ident("command"))
        .unwrap();

    let ocmd = command_attr
        .parse_args::<Command>()
        .map_err(|e| {
            can_impl = false;
            e.to_compile_error().to_tokens(into);
            e
        })
        .ok();
    if let Some(ref c) = ocmd {
        if c.long_desc.len() > 0 {
            helpers_template.long_desc = c.long_desc.as_str();
        }
        if c.short_desc.len() > 0 {
            helpers_template.short_desc = c.short_desc.as_str();
        }
    }
    let helpers_code = helpers_template
        .render()
        .map_err(|_e| {
            panic!("failed to render the helpers impl template");
        })
        .unwrap();

    //eprintln!("{}", helpers_code);

    syn::parse_str::<ItemImpl>(&helpers_code)
        .map_err(|_e| panic!("failed to parse helpers code as an ItemImpl"))
        .unwrap()
        .to_tokens(into);
}

#[derive(Debug)]
enum LitIdent {
    Lit(Literal),
    Ident(Ident),
}

impl fmt::Display for LitIdent {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        let s = match self {
            LitIdent::Lit(l) => l.to_string(),
            LitIdent::Ident(i) => i.to_string(),
        };
        write!(f, "{}", s)
    }
}

const COMMAND_ATTR: &'static str = "command";
const OPTION_ATTR: &'static str = "option";
const ARG_ATTR: &'static str = "arg";
const DEFAULT_ATTR: &'static str = "default";

fn is_command_attr(att: &'_ &Attribute) -> bool {
    att.path.is_ident(COMMAND_ATTR)
}

fn is_field_attr(att: &'_ &Attribute) -> bool {
    let path = &att.path;
    path.is_ident(ARG_ATTR) || path.is_ident(OPTION_ATTR) || path.is_ident(DEFAULT_ATTR)
}

fn dump_code(code: &str, append: bool) {
    let od = env::var("CLIGEN_PARSE_FILE").unwrap_or_else(|_e| "".into());
    if od.is_empty() {
        return;
    }
    let mut oo = OpenOptions::new();
    oo.write(true);
    oo.create(true);
    if !append {
        oo.truncate(true);
    } else {
        oo.append(true);
    }
    if let Ok(mut file) = oo.open(&od) {
        file.write_all(code.as_bytes());
    }
}
