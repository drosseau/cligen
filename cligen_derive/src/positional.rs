use proc_macro2::Ident;
use quote::quote;
use std::fmt;
use syn::parse::{self, Parse, ParseStream};
use syn::Field;
use syn::{Lit, LitBool, LitInt, LitStr, Token};

use super::{lit_to_string, quote_string};

#[derive(Clone)]
pub struct Positional {
    pub meta: String,
    pub pos: i32,
    pub autocomplete: Option<String>,
    pub default: Option<String>,
    pub action: Option<String>,
    pub validate: Option<String>,
    pub required: bool,
    pub field: String,
    pub ty: String,
}

impl Positional {
    pub fn update_field(&mut self, field: &Field) -> parse::Result<()> {
        self.field = field.ident.as_ref().unwrap().to_string();
        let ty = &field.ty;
        self.ty = quote! {#ty}.to_string();
        Ok(())
    }

    pub fn is_static_str(&self) -> bool {
        self.ty.contains("'static str")
    }
}

impl fmt::Display for Positional {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "{}: {}", self.field, self.ty)
    }
}

impl Default for Positional {
    fn default() -> Self {
        Self {
            meta: Default::default(),
            pos: Default::default(),
            autocomplete: Default::default(),
            default: Default::default(),
            action: Default::default(),
            validate: Default::default(),
            required: true,
            field: Default::default(),
            ty: Default::default(),
        }
    }
}

impl Parse for Positional {
    fn parse(buf: ParseStream) -> parse::Result<Self> {
        let mut p: Self = Default::default();
        loop {
            let ident: Ident = buf.parse()?;
            let _eq: Token![=] = buf.parse()?;
            let name = ident.to_string();
            match name.as_str() {
                "pos" => {
                    let i: LitInt = buf.parse()?;
                    p.pos = i.base10_parse()?;
                }
                "meta" => {
                    let ls: LitStr = buf.parse()?;
                    let s = ls.value();
                    p.meta = quote_string(&s);
                }
                "action" => {
                    let s: syn::Ident = buf.parse()?;
                    p.action = Some(s.to_string());
                }
                "autocomplete" => {
                    let s: syn::Ident = buf.parse()?;
                    p.autocomplete = Some(s.to_string());
                }
                "required" => {
                    let b: LitBool = buf.parse()?;
                    p.required = b.value;
                }
                "validate" => {
                    let s: syn::Ident = buf.parse()?;
                    p.validate = Some(s.to_string());
                }
                "default" => {
                    let s: Lit = buf.parse()?;
                    p.default = Some(lit_to_string(&s));
                }
                _ => {
                    return Err(parse::Error::new(
                        ident.span(),
                        format!("unexpected attribute for positional: {}", &name),
                    ))
                }
            }
            if buf.is_empty() {
                break;
            }
            if !buf.peek(Token![,]) {
                return Err(buf.error("expected ,"));
            } else {
                let _comma: Token![,] = buf.parse()?;
            }
        }
        Ok(p)
    }
}
