use cligen::{cligen_autocomplete, cligen_main, Command, CommandError, CommandResult};
use cligen_derive::*;
use std::borrow::Cow;

fn say_hello(args: &SayHello) -> CommandResult {
    if args.name.len() == 0 {
        return CommandError::new_err("can't say hi to no one!", -1);
    }
    if args.name == "Earth" {
        return CommandError::new_err("I think you meant World", -1);
    }
    let to = match args.prefix {
        Some(pre) => Cow::Owned(format!("{} {}", pre, args.name)),
        None => Cow::Borrowed(args.name),
    };
    println!("Hello {}", to);
    if args.twice {
        println!("Hello {}", to);
    }
    Ok(0)
}

fn validate_hello_name(name: &str) -> bool {
    name != "Earth"
}

#[derive(Command)]
#[command(func = say_hello)]
struct SayHello {
    #[option(
        short = 'p',
        long = "prefix",
        help = "Prefix for the thing to say hi to",
        allowed = ["Dr", "Mr", "Ms", "Mrs"]
    )]
    prefix: Option<&'static str>,
    #[option(short = 't', long = "twice", help = "Say hi twice!")]
    twice: bool,
    #[arg(pos = 0, meta = "TO", validate = validate_hello_name)]
    name: &'static str,
}

/*
struct SayHello {
    prefix: Option<String>,
    twice: bool,
    name: String,
}
*/

fn main() {
    cligen_autocomplete!(SayHello);
    cligen_main!(SayHello, no_autocomplete);
}

#[cfg(test)]
mod tests {}
