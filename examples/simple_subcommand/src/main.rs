use cligen::{cligen_main, Command, CommandError, CommandResult};
use cligen_derive::*;

#[derive(Command)]
#[command(
    subcommands = [
        "hello" => SayHello,
        Goodbye,
    ],
    long_desc = "Say hello or goodbye to something"
)]
struct Say {
    #[option(short = 'v', long = "verbose", help = "Verbose", default = false)]
    verbose: bool,
}

fn say_hello(args: &SayHello) -> CommandResult {
    if args.name.len() == 0 {
        return CommandError::new_err("can't say hi to no one!", -1);
    }
    if args.name == "Earth" {
        return CommandError::new_err("I think you meant World", -1);
    }
    let to = match args.prefix.as_ref() {
        Some(pre) => {
            format!("{} {}", pre, args.name)
        }
        None => args.name.clone(),
    };
    println!("Hello {}", to);
    if args.twice {
        println!("Hello {}", to);
    }
    Ok(0)
}

#[derive(Command)]
#[command(
    name = "hello", func = say_hello, short_desc = "Say Hello",
    long_desc = "Say hello to something. Blah blh bdfkjdl dflkjsl fkdjs lkadsjf ldskjf aldkfjs flkadsj falfjads fkdsjlfkjladsk fldsj flkdsjf lkdsfj dslkfjlkjds flsdjflkdsf jdslfk djklds fjdskf lsdfj flkjlkjds flkdsfj dslkfj flkj ljkdfl al"
)]
struct SayHello {
    #[option(
        short = 'p',
        long = "prefix",
        help = "Prefix for the thing to say hi to lalalalala sdfksjdfk dfjkdsfkdsjf kfkdsfjdskfj fkdsfj kj kdsj fkdsjfdskfjdskfj kds fdsjkfdskfj kdsfj dkfjkj fkjds fkdsjf skdfj dkfj kfjd fkdsjf kfjsdkfjdsk fkdsjf dskfj dfkjkjkd"
    )]
    prefix: Option<&'static str>,
    #[option(short = 't', long = "twice", help = "Say hi twice!")]
    twice: bool,
    #[arg(pos = 0, required = false, meta = "TO", default = "World")]
    name: String,
}

fn say_goodbye(args: &Goodbye) -> CommandResult {
    if args.name.len() == 0 {
        return CommandError::new_err("can't say bye to no one!", -1);
    }
    if args.name == "Earth" {
        return CommandError::new_err("I think you meant World", -1);
    }
    let to = match args.prefix.as_ref() {
        Some(pre) => {
            format!("{} {}", pre, args.name)
        }
        None => args.name.clone(),
    };
    println!("Goodbye {}", to);
    if args.twice {
        println!("Goodbye {}", to);
    }
    Ok(0)
}

#[derive(Command)]
#[command(func = say_goodbye)]
struct Goodbye {
    #[option(
        short = 'p',
        long = "prefix",
        help = "Prefix for the thing to say bye to"
    )]
    prefix: Option<String>,
    #[option(short = 't', long = "twice", help = "Say hi twice!")]
    twice: bool,
    #[arg(pos = 0, meta = "TO", default = "World")]
    name: String,
}

fn main() {
    cligen_main!(Say);
}

#[cfg(test)]
mod tests {}
